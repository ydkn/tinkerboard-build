#!/bin/bash -e

export BOARD="rk3288-miniarm"
export ARCH="arm"
export DEFCONFIG="miniarm-rk3288_defconfig"
export UBOOT_DEFCONFIG="tinker-rk3288_defconfig"
export DTB="rk3288-miniarm.dtb"
export CROSS_COMPILE="arm-linux-gnueabihf-"
export CHIP="rk3288"
export DEBIAN_ARCH="armhf"
