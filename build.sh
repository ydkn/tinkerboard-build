#!/bin/bash -e

if ! [ $(id -u) = 0 ]; then
  echo "I am not root!"
  exit 1
fi

export ROOT_DIR="$(cd "$(dirname "$0")" ; pwd -P)/rockchip-bsp"

source "${ROOT_DIR}/build/board_configs.sh"

# cleanup
rm -rf ${ROOT_DIR}/out/*
rm -rf ${ROOT_DIR}/rootfs/binary

# u-boot
cd "${ROOT_DIR}"

./build/mk-uboot.sh "${BOARD}"

# kernel
cd "${ROOT_DIR}/kernel"

make clean
rm -f .config

cd "${ROOT_DIR}"

./build/mk-kernel.sh "${BOARD}"

KERNEL_RELEASE="$(cat ${ROOT_DIR}/kernel/include/config/kernel.release)"

cd "${ROOT_DIR}/kernel"

rm -rf ${ROOT_DIR}/out/kernel/modules/*
mkdir -p "${ROOT_DIR}/out/kernel/modules"
make INSTALL_MOD_PATH="${ROOT_DIR}/out/kernel/modules" modules_install

HEADERS_TMP_PATH="${ROOT_DIR}/out/kernel/headers/usr/src/linux-headers-${KERNEL_RELEASE}"
rm -rf "${ROOT_DIR}/out/kernel/headers"
mkdir -p "${HEADERS_TMP_PATH}"
make INSTALL_HDR_PATH="${HEADERS_TMP_PATH}" headers_install
cp Makefile "${HEADERS_TMP_PATH}/"
cp Module.symvers "${HEADERS_TMP_PATH}/"
cp .config "${HEADERS_TMP_PATH}/"
cp -r include/* "${HEADERS_TMP_PATH}/"
ln -s /usr/lib/linux-kbuild-4.9/scripts "${HEADERS_TMP_PATH}/scripts"
find arch \
  -name '*.h' -o \
  -name 'Makefile' -o \
  -name 'Kbuild' -o \
  -name '*.conf' -o \
  -name '.lds' -o \
  -name '*.platforms' -o \
  -name '*.S' -o \
  -name '*.sh' -o \
  -name '*.cpu' -o \
  -name '*.um' | cpio -updm "${HEADERS_TMP_PATH}"

cp "${ROOT_DIR}/out/boot.img" "${ROOT_DIR}/output/${BOARD}-${ARCH}-${KERNEL_RELEASE}-boot.img"

cd "${ROOT_DIR}/out/kernel"

tar -cf modules.tar modules
gzip -9 modules.tar
cp modules.tar.gz "${ROOT_DIR}/output/${BOARD}-${ARCH}-${KERNEL_RELEASE}-modules.tar.gz"

tar -cf headers.tar headers
gzip -9 headers.tar
cp headers.tar.gz "${ROOT_DIR}/output/${BOARD}-${ARCH}-${KERNEL_RELEASE}-headers.tar.gz"

# kernel update package
cd "${ROOT_DIR}/scripts"
tar -cf "${ROOT_DIR}/out/update.tar" install-kernel.sh

cd "${ROOT_DIR}/out"
echo "${KERNEL_RELEASE}" > "${ROOT_DIR}/out/VERSION"
tar -rf "${ROOT_DIR}/out/update.tar" VERSION boot.img

cd "${ROOT_DIR}/out/kernel"
tar -rf "${ROOT_DIR}/out/update.tar" modules.tar.gz headers.tar.gz

gzip -9 "${ROOT_DIR}/out/update.tar"
mv "${ROOT_DIR}/out/update.tar.gz" "${ROOT_DIR}/output/${BOARD}-${ARCH}-${KERNEL_RELEASE}-update.tar.gz"

# rootfs
cd "${ROOT_DIR}/rootfs"

RELEASE=stretch TARGET=base ARCH="${DEBIAN_ARCH}" ./mk-base-debian.sh
ARCH="${DEBIAN_ARCH}" ./mk-rootfs-stretch.sh

cp -r ${ROOT_DIR}/out/kernel/modules/* ${ROOT_DIR}/rootfs/binary/
cp -r ${ROOT_DIR}/out/kernel/headers/* ${ROOT_DIR}/rootfs/binary/

./mk-image.sh

cp "${ROOT_DIR}/rootfs/linaro-rootfs.img" "${ROOT_DIR}/output/${BOARD}-${ARCH}-${KERNEL_RELEASE}-rootfs.img"

# image
cd "${ROOT_DIR}"

./build/mk-image.sh -c "${CHIP}" -t system -r rootfs/linaro-rootfs.img

mv "${ROOT_DIR}/out/system.img" "${ROOT_DIR}/output/${BOARD}-${ARCH}-${KERNEL_RELEASE}-image.img"
