# Tinkerboard Debian Build

Below are the instructions of how to build an image for the Tinkerboard.

## Usage

### Get the build repo

    $ git clone https://gitlab.com/ydkn/tinkerboard-build.git

### Setup build environment

Vagrant is used to provide the build environment.

#### Install vagrant-disksize plugin

    $ vagrant plugin install vagrant-disksize

#### Start the virtual machine

    $ vagrant up

Provisioning of the virtual machine may take a while.

### Build image

    $ vagrant ssh
    $ sudo ./build.sh

### Output artifacts

All outputs are stored in /vagrant/output which should be accessible on the host system.

- *&lt;BOARD&gt;-&lt;ARCH&gt;-&lt;KERNEL_VERSION&gt;-boot.img* boot partition image
- *&lt;BOARD&gt;-&lt;ARCH&gt;-&lt;KERNEL_VERSION&gt;-modules.tar.gz* compiled kernel modules
- *&lt;BOARD&gt;-&lt;ARCH&gt;-&lt;KERNEL_VERSION&gt;-headers.tar.gz* kernel headers
- *&lt;BOARD&gt;-&lt;ARCH&gt;-&lt;KERNEL_VERSION&gt;-update.tar.gz* update package for existing installations
- *&lt;BOARD&gt;-&lt;ARCH&gt;-&lt;KERNEL_VERSION&gt;-rootfs.img* root partition image
- *&lt;BOARD&gt;-&lt;ARCH&gt;-&lt;KERNEL_VERSION&gt;-image.img* complete image to flash on SD/MMC card

## Credits

Based on https://gist.github.com/TinkerTeam/6286550ce70d34f6b3d483cd803da786 and https://github.com/radxa/rockchip-bsp
